//
//  DetailPage.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 15/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit

class DetailPage: UIViewController,UITableViewDelegate , UITableViewDataSource {
    @IBOutlet var HeaderView: UIView!
    
    @IBOutlet var _imageView: UIImageView!
    @IBOutlet var _tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.tableHeaderView = HeaderView
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row != 1 ? UITableViewAutomaticDimension : 100
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != 1 {
          
            var cell : LblCell! = tableView.dequeueReusableCell(withIdentifier: "LblCell") as? LblCell
            if cell == nil {
                tableView.register(UINib(nibName: "LblCell", bundle: nil), forCellReuseIdentifier: "LblCell")
                cell = tableView.dequeueReusableCell(withIdentifier: "LblCell", for: indexPath) as! LblCell
            }
            
            
            return cell
        }
        else {
            var cell : AddToCart! = tableView.dequeueReusableCell(withIdentifier: "AddToCart") as? AddToCart
            if cell == nil {
                tableView.register(UINib(nibName: "AddToCart", bundle: nil), forCellReuseIdentifier: "AddToCart")
                cell = tableView.dequeueReusableCell(withIdentifier: "AddToCart", for: indexPath) as! AddToCart
            }
            return cell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
