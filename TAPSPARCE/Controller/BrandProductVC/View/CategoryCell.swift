//
//  CategoryCell.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 15/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    @IBOutlet var _view: UIView!
    @IBOutlet var _lblName: UILabel!
    @IBOutlet var _imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
