//
//  BrandProductVC.swift
//  TAPSPARCE
//
//  Created by Raj Kumar on 15/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD
class BrandProductVC: UIViewController ,UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , HeaderViewDelegate {
    @IBOutlet var _topHeaderView: UIView!
    @IBOutlet var _headerView: HeaderView!
    
    @IBOutlet var _collectionView: UICollectionView!
     var cat: cartegory?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _collectionView.register(UINib(nibName: "CategoryCell", bundle:nil), forCellWithReuseIdentifier: "CategoryCell")
        _collectionView.register(UINib(nibName: "CollectionHeader", bundle:nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionHeader")

        _collectionView.delegate = self
        _collectionView.dataSource = self
        _collectionView.backgroundColor = UIColor.clear
        
        
        _headerView = Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)![0] as! HeaderView
        _headerView.frame = CGRect(x: 0, y: 0, width: _topHeaderView.frame.size.width, height: 110)
        _topHeaderView.addSubview(_headerView)
        _headerView.delegate = self
        UIManager.share.setDropShadowToVew(view: _topHeaderView)
        //self.getBrandProductFromServer()

        // Do any additional setup after loading the view.
    }
    
    func getBrandProductFromServer(){
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Loading...."
        APIManager.shared.getRequestCategory(values: ProductWaise + "12") { (error, json) in
            DispatchQueue.main.async {
                if error == nil{
                    if json?.jsonDic!["sub_categories"] != nil && json?.jsonDic!["sub_categories"] is NSNull == false{
                        self.cat = try! cartegory.init(json: (json?.jsonDic!["sub_categories"]! as! NSArray)[0] as! [String : Any])
                        self._collectionView.reloadData()
                    }
                }
                 MBProgressHUD.hide(for: self.view, animated: true)
            }
    }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK :- HeaderDelegate Method
    func didCartActionClick() {
        print("Cart Click")
    }
    func didWishlistActionClick() {
        print("WishList Click")
        
    }
    func didSlideActionClick() {
        print("Slide Click")
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    //MARK :- UICollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cat?.subCategory == nil ? 0 : (self.cat?.subCategory.count)!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0);  // top, left, bottom, right
    }
   

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 42) / 2 , height: 230)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CollectionHeader", for: indexPath) as! CollectionHeader
            let imageURl =  cat?.cartegory.Image == nil ? "" : categoryImageUrl +  (cat?.cartegory.Image)!
            headerView._imageView.sd_setImage(with:  URL(string: imageURl), placeholderImage: #imageLiteral(resourceName: "placeHolder"))

            return headerView

        default:
            fatalError("Unexpected element kind")

            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        UIManager.share.setShadowToVew(view: cell._view)
        
        let dict = self.cat?.subCategory[indexPath.item] as! sub_cartegory
        cell._lblName.text = dict.sub_cartegory.name
        //cell._lblNumberOfProducts.text = ""
         cell._imageView.sd_setImage(with:  URL(string: categoryImageUrl +  dict.sub_cartegory.Image), placeholderImage: #imageLiteral(resourceName: "placeHolder"))
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        let detailPage :  DetailPage = DetailPage(nibName: "DetailPage", bundle: nil)
        
        self.navigationController?.pushViewController(detailPage, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
