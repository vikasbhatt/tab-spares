//
//  CartVC.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 13/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit
class CartVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var dataCart: [Cart] = []
    @IBOutlet var _tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        _tableView.dataSource = self
        _tableView.delegate = self
        dataCart = DBManager.shared.fetchCartFromCoreData()
        _tableView.rowHeight = UITableViewAutomaticDimension
        _tableView.estimatedRowHeight = 45
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  dataCart.count
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CartCell"
        var  Cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? CartCell
        if Cell == nil {
            // register tableView cell
            tableView.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: identifier)
            Cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? CartCell
        }
        Cell?._lblProdName.text =  dataCart[indexPath.row].productName
        Cell?._lblQty.text =  String(describing: dataCart[indexPath.row].qty)
        Cell?._btnAdd.addTarget(self, action: #selector(self.addToCart(_:)), for: UIControlEvents.touchUpInside)
        Cell?._btnMinus.addTarget(self, action: #selector(self.removeFromCart(_:)), for: UIControlEvents.touchUpInside)
        return Cell!
    }
    
    @objc func addToCart(_ sender:UIButton){
        let data = dataCart[sender.tag]
        data.qty =   data.qty + 1
        self.checkCondition(data: data)
    }
    
    func checkCondition(data : Cart){
        let json : [String : Any] = ["item_id" : data.item_id,"sku" : data.sku as Any,"qty" : data.qty,"name" : data.productName as Any,"price" : data.price as Any,"product_type" : data.product_type as Any,"quote_id" : data.quote_id as Any , "status" : false]
        DBManager.shared.saveAgainstSKU(json: json)
        _tableView.reloadData()

    }
    @objc func removeFromCart(_ sender:UIButton){
        let data = dataCart[sender.tag]
        data.qty =   data.qty - 1
       self.checkCondition(data: data)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
