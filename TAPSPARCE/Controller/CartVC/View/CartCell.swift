//
//  CartCell.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 13/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet var _lblQty: UILabel!
    @IBOutlet var _lblProdName: UILabel!
    @IBOutlet var _btnMinus: UIButton!
    @IBOutlet var _btnAdd: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
