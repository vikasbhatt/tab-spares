//
//  DashBoardVC.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 14/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit
import MBProgressHUD
import MaterialComponents.MaterialSnackbar
class DashBoardVC: UIViewController,UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , HeaderViewDelegate{
  
    @IBOutlet var _topHeaderView: UIView!
    @IBOutlet var _headerView: HeaderView!
    var dash: DashModel?
    @IBOutlet var _collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _collectionView.register(UINib(nibName: "DashBordCell", bundle:nil), forCellWithReuseIdentifier: "DashBordCell")
        _collectionView.register(UINib(nibName: "DashboardHeaderCell", bundle:nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "DashboardHeaderCell")
        _collectionView.delegate = self
        _collectionView.dataSource = self
        _collectionView.backgroundColor = UIColor.clear
        _headerView = Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)![0] as! HeaderView
        _headerView.frame = CGRect(x: 0, y: 0, width: _topHeaderView.frame.size.width, height: 110)
        _topHeaderView.addSubview(_headerView)
        _headerView.delegate = self
        UIManager.share.setDropShadowToVew(view: _topHeaderView)
        self.getDashBordDataFromServer()
        // Do any additional setup after loading the view.
    }
    
    func getDashBordDataFromServer(){
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Loading...."
        APIManager.shared.getRequestCategory(values: Home) { (error, json) in
            DispatchQueue.main.async {
                if error == nil{
                    let data = json?.jsonDic!["home"] as! NSDictionary
                    let dataModel = data["data"] as! NSDictionary
                    self.dash = try! DashModel.init(json: dataModel as! [String : Any])
                    self._collectionView.reloadData()
                }
                else{
                    self.showSnakBar()
                }
            MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    //MARK :- HeaderDelegate Method
    func didCartActionClick() {
        print("Cart Click")
    }
    func didWishlistActionClick() {
        print("WishList Click")

    }
    func didSlideActionClick() {
        print("Slide Click")

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK :- UICollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.dash?.home.Category == nil ? 10 :(self.dash?.home.Category.count)!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0);  // top, left, bottom, right
    }
    
    func showSnakBar(){
        let action = MDCSnackbarMessageAction()
        let actionHandler = {() in
            let answerMessage = MDCSnackbarMessage()
            answerMessage.text = "Loading..."
            MDCSnackbarManager.show(answerMessage)
            DispatchQueue.main.async {
                 self.getDashBordDataFromServer()
            }
        }
        action.handler = actionHandler
        action.title = "Retry"
        let message = MDCSnackbarMessage()
        message.text = snakeBarMassage
        message.action = action
        MDCSnackbarManager.show(message)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 42) / 2 , height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "DashboardHeaderCell", for: indexPath)
            return headerView
            
        default:
            fatalError("Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashBordCell", for: indexPath) as! DashBordCell
        UIManager.share.setShadowToVew(view: cell._view)
        guard self.dash?.home == nil else {
            let dict = self.dash?.home.Category[indexPath.item] as! sub_cartegory
            cell._imageView.sd_setImage(with:  URL(string: categoryImageUrl +  dict.sub_cartegory.Image), placeholderImage: #imageLiteral(resourceName: "placeHolder"))
            cell._lblBrandName.text = dict.sub_cartegory.name
            cell._lblNumberOfProducts.text = dict.sub_cartegory.description + " Products"
            return cell
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         guard self.dash?.home == nil else {
         let dict = self.dash?.home.Category[indexPath.item] as! sub_cartegory
         self.getBrandProductFromServer(id : dict.sub_cartegory.Id)
         return
        }
    }
    
    func getBrandProductFromServer(id : String){
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Loading...."
        APIManager.shared.getRequestCategory(values: ProductWaise + id) { (error, json) in
            DispatchQueue.main.async {
                if error == nil{
                    if json?.jsonDic!["sub_categories"] != nil && json?.jsonDic!["sub_categories"] is NSNull == false{
                       let cat = try! cartegory.init(json: (json?.jsonDic!["sub_categories"]! as! NSArray)[0] as! [String : Any])
                        let viewObject:BrandProductVC = BrandProductVC(nibName: "BrandProductVC", bundle: nil)
                        viewObject.cat = cat
                        self.navigationController?.pushViewController(viewObject, animated: true)
                    }
                }
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
