//
//  DashBordCell.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 15/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit

class DashBordCell: UICollectionViewCell {
    @IBOutlet var _lblNumberOfProducts: UILabel!
    @IBOutlet var _lblBrandName: UILabel!
    @IBOutlet var _imageView: UIImageView!
    @IBOutlet var _view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
