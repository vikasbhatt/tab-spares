//
//  APIManager.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 14/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//
import UIKit
import Alamofire
import SystemConfiguration
import Toast_Swift

@objc class APIManager: NSObject {
    @objc var resuletDict : NSDictionary?
    @objc  var resuletArray : NSDictionary?
    @objc  static let shared = APIManager()
    class Jsondata {
        var jsonDic: NSDictionary?
        var jsonArray: NSArray?
        var jsonString: String?
    }
    
    

    
    //***** call GetMethod ********//
    func getRequestCategory(values:String  , handler: @escaping(NSError?,Jsondata?)->Void) {
        if (self.isInternetAvailable() == false){
            handler(NSError(domain: "com.Orange.TAPSPARCE", code: 0, userInfo: nil),nil)
            return
        }
        Alamofire.request(BaseUrl+values,method: .get, encoding: URLEncoding(destination: .methodDependent))
            .responseJSON { response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        let JSON = response.result.value as! NSDictionary
                        if JSON["status"] as! Bool == true{
                            if JSON["data"] != nil || JSON["data"] is NSNull == false{
                                let handlerData =  Jsondata()
                                if JSON["data"] is NSDictionary{
                                    handlerData.jsonDic = JSON["data"] as? NSDictionary
                                }
                                else{
                                    handlerData.jsonArray = JSON["data"] as? NSArray
                                }
                                handler(nil,handlerData)
                            }
                            else{
                                handler(NSError(domain: "com.Orange.TAPSPARCE", code: 0, userInfo: nil),nil)
                            }
                        }
                            
                        else{
                            handler(NSError(domain: "com.Orange.TAPSPARCE", code: 0, userInfo: nil),nil)
                        }
                        
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        handler(error as NSError?,nil)
                    }
                }
        }
        
        
    }
    
    
    //*** upload image with other parameters ***//
    func uploadImage(values:String,imagesArray:NSArray, postKey : String ,postdata:Parameters ,viewController : UINavigationController , handler: @escaping(NSError?,Jsondata?)->Void) {
        if (self.isInternetAvailable() == false){
            // Toast(text: ConstantClass.networkError, duration: Delay.long).show()
            viewController.view.makeToast("Please Check intenet connection")
            
            handler(NSError(domain: "Orange-Mantra.MedsIndia", code: 0, userInfo: nil),nil)
            return
        }
        
        
        let URL = try! URLRequest(url: BaseUrl+values, method: .post)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (image) in imagesArray {
                if  let imageData = UIImageJPEGRepresentation(image as! UIImage, 0.6) {
                    multipartFormData.append(imageData, withName: postKey, fileName: "image.jpeg", mimeType: "image/jpeg")
                }
            }
            for (key,value) in postdata {
                if let value = value as? String {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
            }
            //print(multipartFormData[BodyPart])
            
        }, with: URL, encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseJSON { response in
                    DispatchQueue.main.async {
                        if response.result.isSuccess{
                            let JSON = response.result.value as! NSDictionary
                            let handlerData =  Jsondata()
                            handlerData.jsonDic = JSON
                            handler(nil,handlerData)
                        }
                        else{
                            viewController.view.makeToast("image not uploaded")
                            handler(NSError(domain: "Orange-Mantra.MedsIndia", code: 0, userInfo: nil),nil)
                        }
                        
                    }  // original URL request
                }
                
            case .failure(let encodingError):
                DispatchQueue.main.async {
                    viewController.view.makeToast(encodingError.localizedDescription)
                    handler(encodingError as NSError?,nil)
                }
            }
        })
        
        
    }

    
    
    
    //***** check network avilability ***//
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
}

