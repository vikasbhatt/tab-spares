//
//  ConstentFile.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 14/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import Foundation

let BaseUrl : String = "http://demo.mobilityenterprisegroup.com/"
let categoryImageUrl = "http://demo.mobilityenterprisegroup.com/pub/media/catalog/category/"
let ProductImageUrl = "http://demo.mobilityenterprisegroup.com/pub/media/catalog/product/"
let dataNotFound : String = "Data Not found"
let snakeBarMassage : String = "Server not responding, please retry"

let cartID = "cartID"

// sub url

let getCartUrl = "cart.php?q=cart_data&cartid="
let cartCreate = "cart.php?q=create_cart&username"
let CatProductWaise = "categoryall.php?q=category_product&categoryid="
let ProductWaise = "categoryall.php?q=sub_categories&categoryid="
let Home = "homepageapi.php?q=home"
