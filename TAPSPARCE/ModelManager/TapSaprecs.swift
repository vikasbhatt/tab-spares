//
//  TapSaprecs.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 14/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit

@objc class TapSaprecs: NSObject {
    @objc  static let share = TapSaprecs()
    let defaults = UserDefaults.standard
    
    func setUserDefault(value: String , key : String){
        self.defaults.set(value, forKey: key)
        self.defaults.synchronize()
    }
    
    func cartId() -> String{
        return defaults.object(forKey: cartID) != nil ? defaults.object(forKey: cartID) as! String :  "0"
    }
    
    
    func resetDefaultValue(){
       defaults.removeObject(forKey: cartID)
    }
}
