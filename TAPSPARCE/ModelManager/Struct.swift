//
//  Struct.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 13/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import Foundation

struct cart {
    let cartId : String
    let price : Double
    let product_type : String
    let productName : String
    var qty : Int64
    let item_id : Int64
    let quote_id : String
    let sku : String
    let userid : String
    let status : Bool
}

extension cart{
    init(json : [String : Any]) throws {
        self.cartId = TapSaprecs.share.cartId()
        self.price =  json["price"] != nil ? json["price"] as! Double : 0.0
        self.product_type = json["product_type"] as! String
        self.qty =  json["qty"] is Int64 ? json["qty"] as! Int64 : Int64(json["qty"] as! Int)
        self.item_id =  json["item_id"] is Int64 ? json["item_id"] as! Int64 : Int64(json["item_id"] as! Int)
        self.productName = json["name"] != nil ? json["name"] as! String : ""
        self.sku = json["sku"] as! String
        self.userid = "123"
        self.quote_id = json["quote_id"] as! String
        self.status = json["status"] != nil ? json["status"] as! Bool  : true
    }
}


struct cartegory {
    let cartegory : (name : String, Image : String , description : String)
    let subCategory : NSMutableArray
    
}
struct sub_cartegory {
    let sub_cartegory : (name : String, Image : String , description : String, Id : String,brand_Level : String,level : String,sku: String,price : String,product_url : String)
    
}

extension cartegory{
    init(json : [String : Any]) throws {
        let name = json["name"] == nil ? "" : json["name"] as! String
        let Image = json["image-url"] == nil ? "" : json["image-url"] as! String
        let description = json["description"] == nil ? "" : json["description"] as! String
        let _cartegory = (name,Image,description)
        self.cartegory = _cartegory
        var  dict = NSDictionary()
        if json["subcategory"] != nil
        {
            dict = json["subcategory"] is NSNull == false ? json["subcategory"] as! NSDictionary : [:]
        }
        else if json["product"] != nil {
            dict = json["product"] is NSNull == false ? json["product"] as! NSDictionary : [:]
        }
        let keys = dict.allKeys
        let subArray = NSMutableArray()
        for i in 0..<keys.count{
            let sub = try! sub_cartegory.init(json: dict[keys[i]] as! [String : Any])
            subArray.add(sub)
        }
        self.subCategory = subArray
    }
}
/**
 * for category
 *
 */
extension sub_cartegory{
    init(json : [String : Any]) throws {
        let name = json["name"] == nil ? "" : json["name"] as! String
        let Image = json["image-url"] == nil ? "" : json["image-url"] as! String
        let description = json["description"] == nil ? "" : json["description"] as! String
        let Id = (json["id"] == nil || json["id"] is NSNull) ? "" : json["id"] as! String
        let brand_Level = (json["brand-level"] == nil || json["brand-level"] is NSNull) ? "" : json["brand-level"] as! String
        let level = json["level"] == nil ? "" : json["level"] as! String
        let _sku = json["sku"] == nil ? "" : json["sku"] as! String
        let _price = json["price"] == nil ? "" : json["price"] as! String
        let _product_url = json["product_url"] == nil ? "" : json["product_url"] as! String
        let _cartegory = (name,Image,description,Id,brand_Level,level,_sku,_price,_product_url)
        self.sub_cartegory = _cartegory
    }
}

/** parsing
 * for Detail page
 *
 */
extension sub_cartegory{
    init(DetailPage : [String : Any]) throws {
        
        let category_list = DetailPage["category_list"] as! NSDictionary
        
        let name = category_list["name"] == nil ? "" : category_list["name"] as! String
        let Image = category_list["image-url"] == nil ? "" : category_list["image-url"] as! String
        let custom_attributes = category_list["custom_attributes"] as! NSArray
      
        var resultPredicate = NSPredicate(format: "attribute_code contains[c]%@","meta_keyword")
        let filteredArray = custom_attributes.filtered(using: resultPredicate)
        
        
        let description = category_list["description"] == nil ? "" : category_list["description"] as! String
        let Id = (category_list["id"] == nil || category_list["id"] is NSNull) ? "" : category_list["id"] as! String
        let brand_Level = (category_list["brand-level"] == nil || category_list["brand-level"] is NSNull) ? "" : category_list["brand-level"] as! String
        let level = category_list["level"] == nil ? "" : category_list["level"] as! String
        let _sku = category_list["sku"] == nil ? "" : category_list["sku"] as! String
        let _price = category_list["price"] == nil ? "" : category_list["price"] as! String
        let _product_url = category_list["product_url"] == nil ? "" : category_list["product_url"] as! String
        
        
        let _cartegory = (name,Image,description,Id,brand_Level,level,_sku,_price,_product_url)
        self.sub_cartegory = _cartegory
    }
}

/** parsing
 * for DashBoard page
 *
 */
extension sub_cartegory{
    init(Home : [String : Any]) throws {
        let Id = (Home["id"] == nil || Home["id"] is NSNull) ? "" : Home["id"] as! String
        let name = Home["name"] == nil ? "" : Home["name"] as! String
        var Image = String()
        if Home["image-url"] != nil{
            Image = Home["image-url"] is NSNull == true ? "" : Home["image-url"] as! String
        }
        else if Home["img-file"] != nil{
            Image = Home["img-file"] is NSNull == true ? "" : Home["img-file"] as! String
        }
        var  description  = String()
        if Home["totalproduct"] != nil{
            description = Home["totalproduct"] is NSNull == true ? "" : String(describing: Home["totalproduct"] as! NSNumber)
        }
        else if Home["content"] != nil{
            description = Home["content"] is NSNull == true ? "" : Home["content"] as! String
        }
        let brand_Level = (Home["brand-level"] == nil || Home["brand-level"] is NSNull) ? "" : Home["brand-level"] as! String
        let level = Home["level"] == nil ? "" : Home["level"] as! String
        let _sku = Home["sku"] == nil ? "" : Home["sku"] as! String
        let _price = Home["price"] == nil ? "" : Home["price"] as! String
        let _product_url = Home["product_url"] == nil ? "" : Home["product_url"] as! String
        let _cartegory = (name,Image,description,Id,brand_Level,level,_sku,_price,_product_url)
        self.sub_cartegory = _cartegory
    }
}


struct DashModel {
    let home : (topMenu : NSMutableArray, slider : NSMutableArray , Category : NSMutableArray)
}

extension DashModel {
    init(json : [String : Any]) throws {
        let _topMenu =  json["tomenu"] != nil ? json["tomenu"] as! NSArray : []
        let _slider =   json["slider"] != nil ? json["slider"] as! NSArray : []
        let _categories = json["categories"] != nil ? json["categories"] as! NSArray : []
        let topArray = NSMutableArray()
        let sliderArray = NSMutableArray()
        let categoriesArray = NSMutableArray()
        for i in 0..<_topMenu.count{
            let item = try! sub_cartegory.init(Home: _topMenu[i] as! [String : Any])
            topArray.add(item)
        }
        for i in 0..<_slider.count{
            let item = try! sub_cartegory.init(Home: _slider[i] as! [String : Any])
            sliderArray.add(item)
        }
        for i in 0..<_categories.count{
            let item = try! sub_cartegory.init(Home: _categories[i] as! [String : Any])
            categoriesArray.add(item)
        }
        self.home = (topArray,sliderArray,categoriesArray)
        
    }
}






