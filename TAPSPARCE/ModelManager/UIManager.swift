//
//  UIManager.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 15/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit

class UIManager: NSObject {
static let share = UIManager()
    
    func setShadowToVew(view : UIView){
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.7
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 2
        view.layer.cornerRadius = 5
    }
    
    
    func setDropShadowToVew(view : UIView){
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 2
    }
}
