//
//  DBManager.swift
//  TAPSPARCE
//
//  Created by Vinit Chaoudhary on 13/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit

import CoreData
class DBManager: NSObject {
    static let shared = DBManager()
    var cart_Item = [Cart]()
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    var managedContext = NSManagedObjectContext()
    
    override init(){
        super.init()
        if #available(iOS 10.0, *) {
            self.managedContext = CoreDataModel.shared.persistentContainer.viewContext
        } else {
            self.managedContext = CoreDataModel.shared.managedObjectContext
        }
    }
    
    /** check whether SKU is Allready in DataBase
     * if exist update the count of quntity
     * otherwise save new record
     */
    
    func saveAgainstSKU(json : [String : Any]){
        var cartData = try! cart.init(json: json)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart")
        let predicate = NSPredicate(format: "sku == %@", cartData.sku)
        request.predicate = predicate
        request.fetchLimit = 1
        do{
            let count = try managedContext.count(for: request)
            if(count > 0){
                // update qty
              let data =  try managedContext.fetch(request) as! [Cart]
              let user = data.first
                user?.qty = cartData.qty == 0 ? user!.qty + 1 : user!.qty
                user?.status = cartData.status
               self.SaveDB()
            }
            else{
                // save new record
                cartData.qty = cartData.qty == 0 ?  1 : cartData.qty
                self.SaveCartToCoreData(cartData: cartData)
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    //***** Save Data TO Cart ***********//
    func SaveCartToCoreData(cartData : cart ){
        let entity = NSEntityDescription.entity(forEntityName: "Cart",  in: managedContext)!
        let object = Cart (entity: entity,  insertInto: managedContext)
        object.item_id = cartData.item_id
        object.price = cartData.price
        object.cartId = cartData.cartId
        object.qty = cartData.qty
        object.sku = cartData.sku
        object.productName = cartData.productName
        object.userid = cartData.userid
        object.product_type = cartData.product_type
        object.quote_id = cartData.quote_id
        object.status = cartData.status
        self.SaveDB()
    }
    
    
    func SaveDB(){
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    //*** get Cart from coredata **//
    func fetchCartFromCoreData() -> [Cart]{
        var cart = [Cart]()
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Cart")
        do {
            cart = try managedContext.fetch(fetchRequest) as! [Cart]
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return cart
    }
    
    
    //*** get Cart from coredata **//
    func deleteCartData(){
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart")
        if #available(iOS 9.0, *) {
            let request = NSBatchDeleteRequest(fetchRequest: fetch)
            do {
                try managedContext.execute(request)
                try managedContext.save()
            } catch {
                print ("There was an error")
            }

        } else {
            // Fallback on earlier versions
        }
    }
    
    //***** get Cart Count ******//
    func cartCount() -> Int{
        let request =
            NSFetchRequest<NSManagedObject>(entityName: "Cart")
        do {
            return try managedContext.count(for: request)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return 0
    }
    
    // ******** Delete object in single Entity ******* //
    func delete_SingleRecord(singleRecord : NSManagedObject) -> Bool{
        managedContext.delete(singleRecord)
        do {
            try managedContext.save()
            return true
        } catch let error as NSError {
            print("Error While Deleting Note: \(error.userInfo)")
            return false
        }
        
    }
}
