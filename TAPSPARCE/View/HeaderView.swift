//
//  HeaderView.swift
//  TAPSPARCE
//
//  Created by Raj Kumar on 15/11/17.
//  Copyright © 2017 OrangeMantra. All rights reserved.
//

import UIKit
@objc public protocol HeaderViewDelegate {
    @objc optional func didSlideActionClick()
    @objc optional func didWishlistActionClick()
    @objc optional func didCartActionClick()

}
class HeaderView: UIView {
    public weak var delegate : HeaderViewDelegate?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBAction func buttonClick(_ sender: UIButton) {
        if sender.tag == 11 {
            
            delegate?.didSlideActionClick!()
        }
        else if sender.tag == 12 {
            delegate?.didWishlistActionClick!()
        }
        else{
            delegate?.didCartActionClick!()
        }
        
    }
    
}
